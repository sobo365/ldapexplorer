package project.ldaptic.connection.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ConnectionException extends RuntimeException {
    public ConnectionException(String message) {
        super(message);
    }
}
