package project.ldaptic.connection.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.ldaptic.connection.dto.ConnectionDTO;
import project.ldaptic.connection.dto.HostParametersDTO;
import project.ldaptic.connection.dto.ParametersDTO;
import project.ldaptic.connection.model.Connection;
import project.ldaptic.connection.repository.Connections;
import project.ldaptic.connection.service.ConnectionService;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/connections")
public class ConnectionController {

    private ConnectionService connectionService;

    public ConnectionController(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @GetMapping()
    public ResponseEntity<List<ConnectionDTO>> getAll() {
        List<ConnectionDTO> connectionDTOS = Connections.connections.stream().map(Connection::asDTO).collect(Collectors.toList());
        return ResponseEntity.ok(connectionDTOS);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ConnectionDTO> get(@PathVariable String id) {
        Connection connection = connectionService.getConnection(id);
        return ResponseEntity.ok(connection.asDTO());
    }

    @PostMapping()
    public ResponseEntity<ConnectionDTO> createConnection(@RequestBody ParametersDTO parametersDTO) {
        Connection connection = connectionService.createConnection(parametersDTO);
        return ResponseEntity.ok(connection.asDTO());
    }

    @DeleteMapping("/{id}")
    public void deleteConnection(@PathVariable String id) {
        connectionService.deleteConnection(id);
    }

    @PostMapping("/test")
    public ResponseEntity<Boolean> testConnection(@RequestBody HostParametersDTO hostParametersDTO) {
        Boolean connectionCheck = connectionService.testConnection(hostParametersDTO);
        return ResponseEntity.ok(connectionCheck);
    }
}
