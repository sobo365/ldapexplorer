package project.ldaptic.connection.service;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.springframework.stereotype.Service;
import project.ldaptic.connection.dto.AuthenticationDTO;
import project.ldaptic.connection.dto.HostParametersDTO;
import project.ldaptic.connection.dto.NetworkParameterDTO;
import project.ldaptic.connection.dto.ParametersDTO;
import project.ldaptic.connection.exception.ConnectionException;
import project.ldaptic.connection.model.Connection;
import project.ldaptic.connection.model.Parameters;
import project.ldaptic.connection.repository.Connections;
import project.ldaptic.connection.util.ConnectionUtil;
import project.ldaptic.jndi.LdapticUtil;
import project.ldaptic.ldap.LdapApi;

import java.util.List;
import java.util.UUID;

@Service
public class ConnectionService {

    private LdapticUtil ldapticUtil;

    public ConnectionService(LdapticUtil ldapticUtil) {
        this.ldapticUtil = ldapticUtil;
    }

    public Connection createConnection(ParametersDTO parametersDTO) {
        Connection connection = LdapApi.createLdapConnection(parametersDTO);
        Connections.connections.add(connection);
        return connection;
    }

    public Connection getConnection(String id) {
        for (Connection conn : Connections.connections) {
            if (conn.getId().equals(UUID.fromString(id))) {
                return conn;
            }
        }
        throw new ConnectionException("Connection with provided id does not exists!");
    }

    public void deleteConnection(String id) {
        for (Connection conn : Connections.connections) {
            if (conn.getId().equals(UUID.fromString(id))) {
                Connections.connections.remove(conn);
                return;
            }
        }
        throw new ConnectionException("Connection with provided id does not exists!");
    }

    public Boolean testConnection(HostParametersDTO hostParametersDTO) {
        Boolean bool = LdapApi.testConnection(hostParametersDTO.getHostname(), hostParametersDTO.getPort());
        return bool;
    }

    public void authenticate(AuthenticationDTO authenticationDTO) {
        if (!ldapticUtil.authenticate(authenticationDTO)) {
            throw new ConnectionException("Invalid credentials");
        }
    }
}
