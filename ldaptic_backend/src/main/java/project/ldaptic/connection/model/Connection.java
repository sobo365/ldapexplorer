package project.ldaptic.connection.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.directory.ldap.client.api.LdapConnection;
import project.ldaptic.connection.dto.ConnectionDTO;

import java.util.UUID;

@ToString
@Getter
@Setter
public class Connection {

    private UUID id;
    private String name;
    private String host;
    private String port;
    private String authenticationType;
    private String userDn;
    private String password;
    private LdapConnection ldapConnection;

    public Connection(UUID id, String name, String host, String port, String authenticationType, String userDn, String password, LdapConnection ldapConnection) {
        this.id = id;
        this.name = name;
        this.host = host;
        this.port = port;
        this.authenticationType = authenticationType;
        this.userDn = userDn;
        this.password = password;
        this.ldapConnection = ldapConnection;
    }

    public ConnectionDTO asDTO() {
        return new ConnectionDTO(id.toString(), name, host, port, authenticationType, userDn, password);
    }
}
