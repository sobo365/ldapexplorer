package project.ldaptic.connection.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class Parameters {

    private String name;
    private String host;
    private String port;
    private String authenticationType;
    private String principal;
    private String credentials;

    public Parameters(String name, String host, String port, String authenticationType, String principal, String credentials) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.authenticationType = authenticationType;
        this.principal = principal;
        this.credentials = credentials;
    }
}
