package project.ldaptic.connection.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class NetworkParameterDTO {

    private String connectionName;
    private String hostname;
    private String port;

    public NetworkParameterDTO() {
    }
}
