package project.ldaptic.connection.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import project.ldaptic.connection.model.Parameters;

@ToString
@Getter
@Setter
public class ParametersDTO {

    private String name;
    private String hostname;
    private Integer port;
    private String authenticationType;
    private String userDn;
    private String password;

    public ParametersDTO() {
    }

}
