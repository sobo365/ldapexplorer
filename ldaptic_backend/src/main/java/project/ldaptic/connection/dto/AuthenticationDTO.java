package project.ldaptic.connection.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AuthenticationDTO {

    private String hostname;
    private String port;
    private String bindDn;
    private String bindPassword;

    public AuthenticationDTO() {
    }
}
