package project.ldaptic.connection.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ConnectionDTO {

    private String id;
    private String name;
    private String hostname;
    private String port;
    private String authenticationType;
    private String userDn;
    private String password;

    public ConnectionDTO(String id, String name, String hostname, String port, String authenticationType, String userDn, String password) {
        this.id = id;
        this.name = name;
        this.hostname = hostname;
        this.port = port;
        this.authenticationType = authenticationType;
        this.userDn = userDn;
        this.password = password;
    }
}
