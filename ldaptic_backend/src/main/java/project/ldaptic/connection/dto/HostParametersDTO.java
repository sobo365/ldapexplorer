package project.ldaptic.connection.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HostParametersDTO {
    private String hostname;
    private Integer port;
}
