package project.ldaptic.jndi;

import org.springframework.stereotype.Component;
import project.ldaptic.connection.dto.AuthenticationDTO;
import project.ldaptic.connection.dto.NetworkParameterDTO;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.ldap.InitialLdapContext;
import java.util.Hashtable;

@Component
public class LdapticUtil {

    public boolean isNetworkReachable(NetworkParameterDTO networkParameter) {
        String url = "ldap://" + networkParameter.getHostname() + ":" + networkParameter.getPort();

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, url);

        try {
            DirContext ctx = new InitialLdapContext(env, null);
            ctx.close();
            System.out.println("Connected: " + url);
            return true;
        } catch (NamingException se) {
            //se.printStackTrace();
            return false;
        }
    }

    public boolean authenticate(AuthenticationDTO authenticationDTO) {
        String url = "ldap://" + authenticationDTO.getHostname() + ":" + authenticationDTO.getPort();

        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, url);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, authenticationDTO.getBindDn());
        env.put(Context.SECURITY_CREDENTIALS, authenticationDTO.getBindPassword());

        try {
            DirContext ctx = new InitialLdapContext(env, null);
            ctx.close();
            System.out.println("Connected: " + url);
            return true;
        } catch (NamingException se) {
            se.printStackTrace();
            return false;
        }
    }

}
