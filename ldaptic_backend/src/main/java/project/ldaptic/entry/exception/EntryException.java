package project.ldaptic.entry.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntryException extends RuntimeException {
    public EntryException(String message) {
        super(message);
    }
}
