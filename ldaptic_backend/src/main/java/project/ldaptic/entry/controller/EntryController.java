package project.ldaptic.entry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.ldaptic.entry.dto.*;
import project.ldaptic.entry.service.EntryService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/entries")
public class EntryController {

    @Autowired
    private EntryService entryService;

    @GetMapping("/{connectionId}")
    public ResponseEntity<List<FullEntryDTO>> getEntries(@PathVariable String connectionId) {
        List<FullEntryDTO> entries = entryService.getEntries(connectionId);
        return ResponseEntity.ok(entries);
    }

    @PostMapping()
    public void addEntry(@RequestBody AddEntryDTO addEntryDTO) {
        entryService.addEntry(addEntryDTO);
    }

    @PostMapping("/delete-entry")
    public ResponseEntity<String> deleteEntry(@RequestBody DeleteEntryDTO deleteEntryDTO) {
        entryService.deleteEntry(deleteEntryDTO);
        return ResponseEntity.ok("OK");
    }

    @PostMapping("/search")
    public ResponseEntity<List<FullEntryDTO>> getEntriesByDn(@RequestBody SearchEntryDTO searchEntryDTO) {
        List<FullEntryDTO> entries = entryService.searchEntries(searchEntryDTO);
        return ResponseEntity.ok(entries);
    }

    @PostMapping("/add-attribute")
    public ResponseEntity<List<String>> addAttribute(@RequestBody AddAttributeDTO addAttributeDTO) {
        List<String> attributes = entryService.addAttribute(addAttributeDTO);
        return ResponseEntity.ok(attributes);
    }

    @PostMapping("/rename-entry")
    public void renameEntry(@RequestBody RenameEntryDTO renameEntryDTO) {
        entryService.renameEntry(renameEntryDTO);

    }

    @PostMapping("/remove-attribute")
    public ResponseEntity<List<String>> removeAttribute(@RequestBody RemoveAttributeDTO removeAttributeDTO) {
        List<String> attributes = entryService.removeAttribute(removeAttributeDTO);
        return ResponseEntity.ok(attributes);
    }

    @PostMapping("/edit-attribute")
    public ResponseEntity<List<String>> editAttribute(@RequestBody EditAttributeDTO editAttributeDTO) {
        List<String> attributes = entryService.editAttribute(editAttributeDTO);
        return ResponseEntity.ok(attributes);
    }
}
