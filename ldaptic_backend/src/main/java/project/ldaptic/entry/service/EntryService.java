package project.ldaptic.entry.service;

import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.ldaptic.connection.model.Connection;
import project.ldaptic.connection.service.ConnectionService;
import project.ldaptic.entry.dto.*;
import project.ldaptic.ldap.LdapApi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EntryService {

    @Autowired
    private ConnectionService connectionService;

    public List<FullEntryDTO> getEntries(String connectionId) {
        List<FullEntryDTO> dtos = new ArrayList<>();
        Connection connection = connectionService.getConnection(connectionId);
        List<Entry> entries = LdapApi.getEntries(connection.getLdapConnection());
        for (Entry e : entries) {
            List<String> attributes = new ArrayList<>();
            e.getAttributes().stream().forEach(a -> attributes.add(a.toString()));
            dtos.add(new FullEntryDTO(e.getDn().getName(), e.getDn().getName(), attributes));

        }
        return dtos;
    }

    public List<FullEntryDTO> searchEntries(SearchEntryDTO searchEntryDTO) {
        List<FullEntryDTO> dtos = new ArrayList<>();
        Connection connection = connectionService.getConnection(searchEntryDTO.getConnectionId());
        List<Entry> entries = LdapApi.searchEntries(connection.getLdapConnection(), searchEntryDTO.getEntryDN());
        for (Entry e : entries) {
            List<String> attributes = new ArrayList<>();
            e.getAttributes().stream().forEach(a -> attributes.add(a.toString()));
            String dn = e.getDn().getName().replace(searchEntryDTO.getEntryDN(), "");
            dtos.add(new FullEntryDTO(dn.substring(0, dn.length() - 1), e.getDn().getName(), attributes));
        }
        return dtos;
    }

    public List<String> addAttribute(AddAttributeDTO addAttributeDTO) {
        Connection connection = connectionService.getConnection(addAttributeDTO.getConnectionId());
        LdapApi.addAttribute(connection.getLdapConnection(), addAttributeDTO.getAttributeKey(), addAttributeDTO.getAttributeValue(), addAttributeDTO.getEntryDn());
        List<String> attributes = getAttributes(connection.getLdapConnection(), addAttributeDTO.getEntryDn());
        return attributes;
    }

    public List<String> getAttributes(LdapConnection ldapConnection, String dn) {
        List<Attribute> attributes = LdapApi.getAttributes(ldapConnection, dn);
        List<String> entryAttributes = attributes.stream().map(Attribute::toString).collect(Collectors.toList());
        return entryAttributes;
    }

    public void addEntry(AddEntryDTO addEntryDTO) {
        Connection connection = connectionService.getConnection(addEntryDTO.getConnectionId());
        LdapApi.addEntry(connection.getLdapConnection(), addEntryDTO.getDn(), addEntryDTO.getAttributes());
    }

    public void deleteEntry(DeleteEntryDTO deleteEntryDTO) {
        Connection connection = connectionService.getConnection(deleteEntryDTO.getConnectionId());
        LdapApi.deleteEntry(connection.getLdapConnection(), deleteEntryDTO.getDn());
        List<String> attributes = getAttributes(connection.getLdapConnection(), deleteEntryDTO.getDn());
    }

    public void renameEntry(RenameEntryDTO renameEntryDTO) {
        Connection connection = connectionService.getConnection(renameEntryDTO.getConnectionId());
        LdapApi.renameEntry(connection.getLdapConnection(), renameEntryDTO.getDn(), renameEntryDTO.getNewDn());
    }

    public List<String> removeAttribute(RemoveAttributeDTO removeAttributeDTO) {
        Connection connection = connectionService.getConnection(removeAttributeDTO.getConnectionId());
        LdapApi.removeAttribute(connection.getLdapConnection(), removeAttributeDTO.getDn(), removeAttributeDTO.getAttributeName());
        List<String> attributes = getAttributes(connection.getLdapConnection(), removeAttributeDTO.getDn());
        return attributes;
    }

    public List<String> editAttribute(EditAttributeDTO editAttributeDTO) {
        Connection connection = connectionService.getConnection(editAttributeDTO.getConnectionId());
        LdapApi.removeAttribute(connection.getLdapConnection(), editAttributeDTO.getDn(), editAttributeDTO.getOldAttribute());
        String attributeKey = editAttributeDTO.getNewAttribute().split(":")[0].trim();
        String attributeValue = editAttributeDTO.getNewAttribute().split(":")[1].trim();
        LdapApi.addAttribute(connection.getLdapConnection(), attributeKey, attributeValue, editAttributeDTO.getDn());
        List<String> attributes = getAttributes(connection.getLdapConnection(), editAttributeDTO.getDn());
        return attributes;
    }
}
