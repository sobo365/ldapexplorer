package project.ldaptic.entry.dto;

import lombok.Getter;

@Getter
public class RenameEntryDTO {
    private String dn;
    private String newDn;
    private String connectionId;
}
