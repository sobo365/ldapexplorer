package project.ldaptic.entry.dto;

import lombok.Getter;

@Getter
public class EditAttributeDTO {
    private String dn;
    private String oldAttribute;
    private String newAttribute;
    private String connectionId;
}
