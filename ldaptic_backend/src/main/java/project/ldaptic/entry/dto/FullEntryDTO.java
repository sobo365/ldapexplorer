package project.ldaptic.entry.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class FullEntryDTO {
    private String dn;
    private String fullDn;
    private List<String> attributes;

    public FullEntryDTO(String dn, String fullDn, List<String> attributes) {
        this.dn = dn;
        this.fullDn = fullDn;
        this.attributes = attributes;
    }

    public FullEntryDTO(String dn, List<String> attributes) {
        this.dn = dn;
        this.attributes = attributes;
    }
}
