package project.ldaptic.entry.dto;

import lombok.Getter;

@Getter
public class SearchEntryDTO {
    private String entryDN;
    private String connectionId;
}
