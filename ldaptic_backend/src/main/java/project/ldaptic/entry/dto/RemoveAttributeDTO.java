package project.ldaptic.entry.dto;

import lombok.Getter;

@Getter
public class RemoveAttributeDTO {
    private String dn;
    private String attributeName;
    private String connectionId;
}
