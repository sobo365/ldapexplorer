package project.ldaptic.entry.dto;

import lombok.Getter;

@Getter
public class AttributeDTO {
    private String key;
    private String value;
}
