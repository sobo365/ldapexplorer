package project.ldaptic.entry.dto;

import lombok.Getter;

@Getter
public class AddAttributeDTO {
    private String entryDn;
    private String attributeKey;
    private String attributeValue;
    private String connectionId;
}
