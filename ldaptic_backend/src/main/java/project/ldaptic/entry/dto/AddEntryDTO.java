package project.ldaptic.entry.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class AddEntryDTO {
    private String dn;
    private List<AttributeDTO> attributes;
    private String connectionId;
}
