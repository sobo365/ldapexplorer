package project.ldaptic.entry.dto;

import lombok.Getter;

@Getter
public class DeleteEntryDTO {
    private String dn;
    private String connectionId;
}

