package project.ldaptic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LdapticApplication {

	public static void main(String[] args) {
		SpringApplication.run(LdapticApplication.class, args);
	}

}
