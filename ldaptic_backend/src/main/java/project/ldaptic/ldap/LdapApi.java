package project.ldaptic.ldap;

import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.entry.*;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import project.ldaptic.connection.dto.ParametersDTO;
import project.ldaptic.connection.exception.ConnectionException;
import project.ldaptic.connection.model.Connection;
import project.ldaptic.entry.dto.AttributeDTO;
import project.ldaptic.entry.exception.EntryException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LdapApi {
    public static void main(String[] args) throws Exception {
        search();
        //deleteEntry();
        // addEntry();
    }

    public static List<Entry> entries = new ArrayList<>();

    public static Connection createLdapConnection(ParametersDTO parametersDTO) {
        try {
            LdapConnection ldapConnection = new LdapNetworkConnection(parametersDTO.getHostname(), parametersDTO.getPort());
            ldapConnection.bind();
            ldapConnection.unBind();
            Connection connection = new Connection(UUID.randomUUID(), parametersDTO.getName(), parametersDTO.getHostname(), parametersDTO.getPort().toString(), parametersDTO.getAuthenticationType(), parametersDTO.getUserDn(), parametersDTO.getPassword(), ldapConnection);
            return connection;
        } catch (Exception e) {
            throw new ConnectionException(e.toString());
        }
    }

    public static void addAttribute(LdapConnection ldapConnection, String attributeKey, String attributeValue, String entryDn) {
        try {
            ldapConnection.bind();
            Modification addEntry = new DefaultModification(ModificationOperation.ADD_ATTRIBUTE, attributeKey, attributeValue);
            ldapConnection.modify(entryDn, addEntry);
        } catch (Exception e) {
            throw new EntryException(e.toString());
        }
    }

    public static void search() throws Exception {
        LdapConnection connection = new LdapNetworkConnection("0.0.0.0", 10389);
        //connection.bind("uid=admin,ou=system", "secret");

        EntryCursor cursor = connection.search("cn=ljudi,dc=example,dc=com", "(objectclass=*)", SearchScope.ONELEVEL, "*");

        for (Entry entry : cursor) {
            entries.add(entry);
        }

        cursor.close();

        System.out.println("++++    Entries:   ++++");
        for (Entry entry : entries) {
            System.out.println(entry.getDn());
        }

        connection.unBind();
    }

    public static List<Entry> getEntries(LdapConnection ldapConnection) {
        try {
            ldapConnection.bind();
            List<Entry> entries = new ArrayList<>();
            EntryCursor cursor = ldapConnection.search("", "(objectclass=*)", SearchScope.ONELEVEL, "*");
            for (Entry entry : cursor) {
                entries.add(entry);
            }
            return entries;
        } catch (Exception e) {
            throw new ConnectionException("Can not bind connection!");
        }
    }

    public static List<Attribute> getAttributes(LdapConnection ldapConnection, String dn) {
        try {
            ldapConnection.bind();
            Entry entry = ldapConnection.lookup(dn);
            List<Attribute> attributes = new ArrayList<>();
            entry.getAttributes().stream().forEach(a -> attributes.add(a));
            ldapConnection.unBind();
            return attributes;
        } catch (Exception e) {
            throw new ConnectionException(e.toString());
        }
    }

    public static List<Entry> searchEntries(LdapConnection ldapConnection, String dn) {
        try {
            ldapConnection.bind();
            List<Entry> entries = new ArrayList<>();
            EntryCursor cursor = ldapConnection.search(dn, "(objectclass=*)", SearchScope.ONELEVEL, "*");
            for (Entry entry : cursor) {
                entries.add(entry);
            }
            return entries;
        } catch (Exception e) {
            throw new ConnectionException(e.toString());
        }
    }

    public static Boolean testConnection(String host, Integer port) {
        LdapConnection connection = new LdapNetworkConnection(host, port);
        try {
            connection.bind();
            connection.unBind();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void addEntry(LdapConnection ldapConnection, String dn, List<AttributeDTO> attributes) {
        DefaultEntry newEntry = new DefaultEntry();
        try {
            newEntry.setDn(dn);
            for (AttributeDTO attr : attributes) {
                newEntry.add(attr.getKey(), attr.getValue());
            }
            ldapConnection.add(newEntry);
        } catch (Exception e) {
            throw new EntryException(e.toString());
        }
    }

    public static void deleteEntry(LdapConnection ldapConnection, String dn) {
        try {
            ldapConnection.delete(dn);
        } catch (Exception e) {
            throw new EntryException(e.toString());
        }
    }

    public static void renameEntry(LdapConnection ldapConnection, String dn, String newDn) {
        try {
            ldapConnection.rename(dn, newDn, true);
        } catch (Exception e) {
            throw new EntryException(e.toString());
        }
    }

    public static void removeAttribute(LdapConnection ldapConnection, String dn, String attribute) {
        String key = attribute.split(":")[0].trim();
        String value = attribute.split(":")[1].trim();
        try {
            Modification removeAttribute = new DefaultModification(ModificationOperation.REMOVE_ATTRIBUTE, key, value);
            ldapConnection.modify(dn, removeAttribute);
        } catch (Exception e) {
            throw new EntryException(e.toString());
        }
    }
}
