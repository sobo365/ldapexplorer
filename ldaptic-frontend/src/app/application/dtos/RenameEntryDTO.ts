export class RenameEntryDTO {
  public dn: String;
  public newDn: String;
  public connectionId: String;
}
