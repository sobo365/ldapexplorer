export class EditAttributeDTO {
    public dn: String;
    public oldAttribute: String;
    public newAttribute: String;
    public connectionId: String;
}