export class AttributeDTO {
  public key: string;
  public value: string;
  public mandatory: boolean;
}
