export class AddAttributeDTO {
    public entryDn: String;
    public attributeKey: String;
    public attributeValue: String;
    public connectionId: String;
}