export class RemoveAttributeDTO {
  public dn: string;
  public attributeName: string;
  public connectionId: String;
}
