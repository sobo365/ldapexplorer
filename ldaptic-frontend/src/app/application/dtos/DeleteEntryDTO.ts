export class DeleteEntryDTO {
  public dn: String;
  public connectionId: String;
}
