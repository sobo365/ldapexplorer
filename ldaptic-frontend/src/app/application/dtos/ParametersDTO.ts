export class ParametersDTO {
    public name: String;
    public hostname: String;
    public port: String;
    public authenticationType: String;
    public userDn: String;
    public password: String;
}