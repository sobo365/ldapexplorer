export class FullEntryDTO {
    public dn: String;
    public fullDn: String;
    public attributes:Array<any>;
}