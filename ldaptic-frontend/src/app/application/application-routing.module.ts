import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrameComponent } from './frame/frame.component';
import { ExplorerComponent } from './explorer/explorer.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { ConnectionsComponent } from './connections/connections.component';
import { HomeComponent } from './home/home.component';
import { HelpComponent } from './help/help.component';
import { LoggerComponent } from './logger/logger.component';


const routes: Routes = [
  {path: '', component: FrameComponent, children : [
    {path: 'explorer',  component: ExplorerComponent},
    {path: 'connections', component: ConnectionsComponent},
    {path: 'home', component: HomeComponent},
    {path: 'help', component: HelpComponent},
    {path: 'logs', component: LoggerComponent}

  ]},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationRoutingModule { }
