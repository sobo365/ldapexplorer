import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';
import { ParametersDTO } from '../../dtos/ParametersDTO';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HostParametersDTO } from '../../connections/dtos/HostParamsDTO';
import { ConnectionsService } from '../../connections/services/connections.service';

@Component({
  selector: 'app-connection-dialog',
  templateUrl: './connection-dialog.component.html',
  styleUrls: ['./connection-dialog.component.css'],
})
export class ConnectionDialogComponent implements OnInit {
  loading = false;

  connectionName = new FormControl('');
  host = new FormControl('');
  port = new FormControl('');
  userDN = new FormControl('');
  password = new FormControl('');
  connectionOk = false;
  connectionError = false;
  hostFormGroup: FormGroup;
  anonymousAuthentication = false;

  constructor(
    private connectionsService: ConnectionsService,
    public dialogRef: MatDialogRef<ConnectionDialogComponent>,
    private _snackBar: MatSnackBar,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.hostFormGroup = this._formBuilder.group({
      nameCtrl: ['', Validators.required],
      hostCtrl: ['', Validators.required],
      portCtrl: new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]+$'),
      ]),
    });
  }

  testConnection() {
    if (this.hostFormGroup.dirty && this.hostFormGroup.valid) {
      const params = new HostParametersDTO();
      params.hostname = this.hostFormGroup.controls['hostCtrl'].value;
      params.port = this.hostFormGroup.controls['portCtrl'].value;
      this.connectionsService.testConnection(params).subscribe(
        (data) => {
          if (data === true) {
            this.connectionOk = true;
            this.connectionError = false;
          } else {
            this.connectionError = true;
            this.connectionOk = false;
          }
        },
        (error) => {
          alert('error');
        }
      );
    } else {
      this.connectionError = true;
    }
  }

  anonymousAuthenticationChange(event) {
    this.anonymousAuthentication = event.checked;
  }

  onSaveClick() {
    let authTypeString = 'Simple';
    if (this.anonymousAuthentication) {
      authTypeString = 'Anonymous';
    }
    this.loading = true;
    const parameters = new ParametersDTO();
    parameters.name = this.hostFormGroup.controls['nameCtrl'].value;
    parameters.hostname = this.hostFormGroup.controls['hostCtrl'].value;
    parameters.port = this.hostFormGroup.controls['portCtrl'].value;
    parameters.authenticationType = authTypeString;
    parameters.userDn = this.userDN.value;
    parameters.password = this.password.value;

    this.connectionsService.addConnection(parameters).subscribe(
      (data) => {
        this.loading = false;
        this.dialogRef.close();
      },
      (error) => {
        this.loading = false;
        this._snackBar.open(error.error.message, 'OK', {
          duration: 5000,
          verticalPosition: 'top',
        });
      }
    );
    console.log(this.hostFormGroup.controls['hostCtrl'].value);
  }
}
