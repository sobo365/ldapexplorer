import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RemoveAttributeDTO } from 'src/app/application/dtos/RemoveAttributeDTO';
import { InternalConnectionService } from 'src/app/application/shared/services/internal-connection.service';
import { EntriesService } from '../../services/entries.service';
import { MatDialog } from '@angular/material/dialog';
import { AddAttributeDialogComponent } from '../../dialogs/add-attribute-dialog/add-attribute-dialog.component';

@Component({
  selector: 'app-attribute-item',
  templateUrl: './attribute-item.component.html',
  styleUrls: ['./attribute-item.component.css'],
})
export class AttributeItemComponent implements OnInit {
  @Input('attribute')
  public attribute: string;

  @Input('entryDn')
  public entryDn: string;

  @Input('index')
  public index;

  @Output()
  attributeOutput = new EventEmitter<any>();

  attributeDisplayName: string;

  listOfAttributeNames = new Map([
    ['cn', 'Common Name'],
    ['ou', 'Organizational Unit'],
    ['dc', 'Domain Component'],
    ['cn', 'Common Name'],
    ['sn', 'Surname'],
    ['mail', 'Email'],
    ['email', 'Email'],
    ['objectclass', 'Object Class'],
    ['ObjectClass', 'Object Class'],
    ['objectClass', 'Object Class'],
    ['uid', 'User Id'],
  ]);

  constructor(
    private internalConnectionService: InternalConnectionService,
    private entryService: EntriesService,
    private dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.convertAttributeName();
  }

  convertAttributeName() {
    if (this.listOfAttributeNames.has(this.attribute.split(':')[0])) {
      this.attributeDisplayName = this.listOfAttributeNames.get(
        this.attribute.split(':')[0]
      );
      this.attributeDisplayName += ': ' + this.attribute.split(':')[1];
    } else {
      this.attributeDisplayName = this.attribute;
    }
  }

  removeAttribute() {
    let params = new RemoveAttributeDTO();
    params.dn = this.entryDn;
    params.attributeName = this.attribute;
    params.connectionId = this.internalConnectionService.getConnections().id;
    console.log(params);
    this.entryService.removeAttribute(params).subscribe(
      (data) => {
        //this.attributeOutput.emit(this.index);
        //console.log(data);
        this.attributeOutput.emit(this.index)
      },
      (error) => {
        alert(error.error.message)
      }
    );
  }

  editAttribute() {
    const dialogRef = this.dialog.open(AddAttributeDialogComponent, {
      data: {
        attribute : this.attribute
      },
    });
    dialogRef.afterClosed().subscribe((result) => this.ngOnInit());
  }
}
