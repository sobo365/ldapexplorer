import { Component, OnInit, Input } from '@angular/core';
import { AttributesService } from '../services/attributes.service';
import { FullEntryDTO } from '../../dtos/FullEntryDTO';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { EntriesService } from '../services/entries.service';
import { AddAttributeDTO } from '../../dtos/AddAttributeDTO';
import { InternalConnectionService } from '../../shared/services/internal-connection.service';
import { ConnectionDTO } from '../../connections/dtos/ConnectionDTO';
import { MatDialog } from '@angular/material/dialog';
import { AddAttributeDialogComponent } from '../dialogs/add-attribute-dialog/add-attribute-dialog.component';
import { DeleteEntryDTO } from '../../dtos/DeleteEntryDTO';

@Component({
  selector: 'app-attributes',
  templateUrl: './attributes.component.html',
  styleUrls: ['./attributes.component.css'],
})
export class AttributesComponent implements OnInit {
  attributes;
  entryDn: any;
  activeConnection: ConnectionDTO;
  convertedDn: any;

  constructor(
    private attributesService: AttributesService,
    private internalConnectionService: InternalConnectionService,
    private dialog: MatDialog,
    private entriesService: EntriesService
  ) {}

  ngOnInit() {
    this.getAttributes();
    this.getEntry();
    this.getConnection();
    this.getConvertedDn();
  }

  getAttributes() {
    this.attributesService.eventEmmiter.subscribe((attributes) => {
      this.attributes = attributes;
    });
  }

  getConvertedDn() {
    this.attributesService.entryConvertedDn.subscribe((dn) => {
      this.convertedDn = dn;
    });
  }

  getEntry() {
    this.attributesService.currentEntryDn.subscribe((entryDn: any) => {
      this.entryDn = entryDn;
    });
  }

  getConnection() {
    this.activeConnection = this.internalConnectionService.getConnections();
  }

  addAttribute() {
    const dialogRef = this.dialog.open(AddAttributeDialogComponent, {
      data: {},
    });
    dialogRef.afterClosed().subscribe((result) => this.ngOnInit());
  }

  attributeLoadAttributes($event) {
    //  console.log($event)
    this.attributes.splice($event, 1);
    // this.attributes = $event;
  }
}
