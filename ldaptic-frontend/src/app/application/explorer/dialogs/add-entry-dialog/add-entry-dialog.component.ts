import {
  Component,
  Inject,
  OnInit,
  ComponentFactoryResolver,
  Type,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { RdnFormComponent } from './rdn-form/rdn-form.component';
import { EntriesService } from '../../services/entries.service';
import { AddEntryDTO } from 'src/app/application/dtos/AddEntryDTO';
import { InternalConnectionService } from 'src/app/application/shared/services/internal-connection.service';
import { ConnectionDTO } from 'src/app/application/connections/dtos/ConnectionDTO';
import { AddAttributeDTO } from 'src/app/application/dtos/AddAttributeDTO';
import { MatStepper } from '@angular/material/stepper';
import { DomPortal } from '@angular/cdk/portal';
import { AttributeDTO } from 'src/app/application/dtos/AttributeDTO';
import { identifierModuleUrl } from '@angular/compiler';
import { RenameEntryDTO } from 'src/app/application/dtos/RenameEntryDTO';
import { EditAttributeDTO } from 'src/app/application/dtos/EditAttributeDTO';

@Component({
  selector: 'app-add-entry-dialog',
  templateUrl: './add-entry-dialog.component.html',
  styleUrls: ['./add-entry-dialog.component.css'],
})
export class AddEntryDialogComponent implements OnInit {
  formGroup: FormGroup;
  rdnFormGroup: FormGroup;
  renameFormGroup: FormGroup;
  @ViewChild('container', { read: ViewContainerRef })
  container: ViewContainerRef;
  components = [];
  dnPreview: string;
  activeConnection: ConnectionDTO;
  isDataAdded = false;
  mainPanel = true;
  objectClassesPanel = false;
  distinguishedNamePanel = false;
  editAttributeState = false;
  attributes = [];
  rdns = [];
  editPosition = -1;
  options: string[] = [
    'cn',
    'ou',
    'dc',
    'sn',
    'ObjectClasss',
    'displayname',
    'givenname',
    'mobile',
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _formBuilder: FormBuilder,
    private entriesService: EntriesService,
    private internalConnectionService: InternalConnectionService,
    private componentFactoryResolver: ComponentFactoryResolver,
    public dialogRef: MatDialogRef<AddEntryDialogComponent>
  ) {
    this.generateMandatoryAttributes();
  }

  ngOnInit(): void {
    this.formGroup = this._formBuilder.group({
      attributeNameCtrl: ['', Validators.required],
      attributeValueCtrl: ['', Validators.required],
    });
    this.rdnFormGroup = this._formBuilder.group({
      rdnNameCtrl: ['', Validators.required],
      rdnValueCtrl: ['', Validators.required],
    });
    this.renameFormGroup = this._formBuilder.group({
      entryDn: ['', Validators.required],
    });
    this.getInternalConnections();
    if (this.data.attriburtes) this.attributes = this.data.attriburtes;
    this.renameFormGroup.setValue({
      entryDn: this.data.entryDn,
    });
  }

  generateMandatoryAttributes() {
    let attribute = new AttributeDTO();
    attribute.key = 'ObjectClass';
    attribute.value = '';
    attribute.mandatory = true;
    let attribute1 = new AttributeDTO();
    attribute1.key = 'ObjectClass';
    attribute1.value = '';
    attribute1.mandatory = true;
    let attribute2 = new AttributeDTO();
    attribute2.key = 'cn';
    attribute2.value = '';
    attribute2.mandatory = true;
    let attribute3 = new AttributeDTO();
    attribute3.key = 'sn';
    attribute3.value = '';
    attribute3.mandatory = true;
    this.attributes.push(attribute);
    this.attributes.push(attribute1);
    this.attributes.push(attribute2);
    this.attributes.push(attribute3);
    console.log(this.attributes);
  }

  backToMainPanel() {
    this.objectClassesPanel = false;
    this.distinguishedNamePanel = false;
    this.mainPanel = true;
  }

  showObjectClassesPanel() {
    this.mainPanel = false;
    this.objectClassesPanel = true;
  }

  addRdnForm() {
    this.mainPanel = false;
    this.distinguishedNamePanel = true;
  }

  addObjectClass() {
    if (this.editAttributeState === true) {
      let attribute = new AttributeDTO();
      attribute.key = this.formGroup.controls['attributeNameCtrl'].value;
      attribute.value = this.formGroup.controls['attributeValueCtrl'].value;
      attribute.mandatory = true;
      this.attributes.splice(this.editPosition, 1);
      this.attributes.push(attribute);
      this.objectClassesPanel = false;
      this.mainPanel = true;
    } else {
      let attribute = new AttributeDTO();
      attribute.key = this.formGroup.controls['attributeNameCtrl'].value;
      attribute.value = this.formGroup.controls['attributeValueCtrl'].value;
      attribute.mandatory = false;
      this.attributes.push(attribute);
      this.objectClassesPanel = false;
      this.mainPanel = true;
    }
    this.editAttributeState = false;
  }

  editAttribute(attr, j) {
    this.editAttributeState = true;
    this.editPosition = j;
    this.formGroup.controls['attributeNameCtrl'].setValue(attr.key);
    this.formGroup.controls['attributeValueCtrl'].setValue(attr.value);
    this.showObjectClassesPanel();
  }

  addRdn() {
    let rdn =
      this.rdnFormGroup.controls['rdnNameCtrl'].value +
      '=' +
      this.rdnFormGroup.controls['rdnValueCtrl'].value;
    this.rdns.push(rdn);
    this.distinguishedNamePanel = false;
    this.mainPanel = true;
    this.generateDnPreview();
  }

  removeRdnFromList(i) {
    this.rdns.splice(i, 1);
    this.generateDnPreview();
  }

  removeAttributeFromList(j) {
    this.attributes.splice(j, 1);
  }

  generateDnPreview() {
    this.dnPreview = '';
    if (this.rdns.length > 0) {
      for (let i = 0; i < this.rdns.length; i++) {
        if (i + 1 == this.rdns.length) {
          this.dnPreview += this.rdns[i] + ',';
          break;
        }
        this.dnPreview += this.rdns[i] + '+';
      }
      this.dnPreview += this.data.parentEntry.fullDn;
    }
  }

  addEntry() {
    let params = new AddEntryDTO();
    params.connectionId = this.activeConnection.id;
    params.attributes = this.attributes;
    params.dn = this.dnPreview;

    console.log(params);

    this.entriesService.addEntry(params).subscribe(
      (data: any) => {
        this.dialogRef.componentInstance.isDataAdded = true;
        this.dialogRef.close();
      },
      (error) => {
        alert('imas error');
        console.log(error);
      }
    );
  }

  renameEntry() {
    let params = new RenameEntryDTO();
    params.connectionId = this.activeConnection.id;
    params.dn = this.data.parentEntry;
    params.newDn = this.renameFormGroup.controls['entryDn'].value;
    console.log(params);
    this.entriesService.renameEntry(params).subscribe(
      (data: any) => {
        this.dialogRef.componentInstance.isDataAdded = true;
        this.dialogRef.close();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getInternalConnections() {
    this.activeConnection = this.internalConnectionService.getConnections();
  }
}
