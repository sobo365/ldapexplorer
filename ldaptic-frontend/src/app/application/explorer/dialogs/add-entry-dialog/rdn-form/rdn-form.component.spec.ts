import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RdnFormComponent } from './rdn-form.component';

describe('RdnFormComponent', () => {
  let component: RdnFormComponent;
  let fixture: ComponentFixture<RdnFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RdnFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RdnFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
