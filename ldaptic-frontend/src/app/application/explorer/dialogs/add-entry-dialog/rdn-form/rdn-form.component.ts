import { Component, OnInit, ComponentRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-rdn-form',
  templateUrl: './rdn-form.component.html',
  styleUrls: ['./rdn-form.component.css'],
})
export class RdnFormComponent implements OnInit {
  formGroup: FormGroup;
  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.formGroup = this._formBuilder.group({
      rdnNameCtrl: ['', Validators.required],
      rdnValueCtrl: ['', Validators.required],
    });
  }

  removeComponent() {}
}
