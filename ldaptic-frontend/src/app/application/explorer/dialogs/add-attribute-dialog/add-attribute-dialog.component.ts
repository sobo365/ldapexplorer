import { Component, OnInit, Inject } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { AddAttributeDTO } from 'src/app/application/dtos/AddAttributeDTO';
import { InternalConnectionService } from 'src/app/application/shared/services/internal-connection.service';
import { ConnectionDTO } from 'src/app/application/connections/dtos/ConnectionDTO';
import { AttributesService } from '../../services/attributes.service';
import { EntriesService } from '../../services/entries.service';
import { EditAttributeDTO } from 'src/app/application/dtos/EditAttributeDTO';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-add-attribute-dialog',
  templateUrl: './add-attribute-dialog.component.html',
  styleUrls: ['./add-attribute-dialog.component.css'],
})
export class AddAttributeDialogComponent implements OnInit {
  formGroup: FormGroup;
  editFormGroup: FormGroup;
  activeConnection: ConnectionDTO;
  entryDn: any;
  errorMessage: string;
  options: string[] = ['cn', 'ou', 'dc', 'sn', 'ObjectClasss', 'displayname', 'givenname', 'mobile'];
  filteredOptions: Observable<string[]>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AddAttributeDialogComponent>,
    private _formBuilder: FormBuilder,
    private internalConnectionService: InternalConnectionService,
    private attributesService: AttributesService,
    private entriesService: EntriesService
  ) {}

  ngOnInit(): void {
    this.getConnection();
    this.getEntry();
    this.formGroup = this._formBuilder.group({
      nameCtrl: ['', Validators.required],
      valueCtrl: ['', Validators.required],
    });
    if (this.data.attribute) {
      this.editFormGroup = this._formBuilder.group({
        editNameCtrl: [
          this.data.attribute.split(':')[0].trim(),
          Validators.required,
        ],
        editValueCtrl: [
          this.data.attribute.split(':')[1].trim(),
          Validators.required,
        ],
      });
    }
    this.filteredOptions = this.formGroup.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  getConnection() {
    this.activeConnection = this.internalConnectionService.getConnections();
  }

  getEntry() {
    this.attributesService.currentEntryDn.subscribe((entryDn) => {
      this.entryDn = entryDn;
    });
  }

  addAttribute() {
    let dto = new AddAttributeDTO();
    dto.attributeKey = this.formGroup.controls['nameCtrl'].value;
    dto.attributeValue = this.formGroup.controls['valueCtrl'].value;
    dto.connectionId = this.activeConnection.id;
    dto.entryDn = this.entryDn.fullDn;
    console.log(dto);
    this.entriesService.addAttribute(dto).subscribe(
      (data: any) => {
        this.attributesService.setAttributes(data);
        this.dialogRef.close();
      },
      (error) => {
        this.errorMessage = error.error.message;
        console.log(error.error.message);
      }
    );
  }

  editAttribute() {
    let dto = new EditAttributeDTO();
    dto.connectionId = this.activeConnection.id;
    dto.dn = this.entryDn.fullDn;
    dto.oldAttribute = this.data.attribute;
    dto.newAttribute =
      this.editFormGroup.controls['editNameCtrl'].value +
      ':' +
      this.editFormGroup.controls['editValueCtrl'].value;
    this.entriesService.editAttribute(dto).subscribe(
      (data: any) => {
        this.attributesService.setAttributes(data);
        this.dialogRef.close();
      },
      (error) => {
        console.log(error);
        this.errorMessage = error.error.message;
      }
    );
  }
}
