import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EntriesService } from './services/entries.service';
import { InternalConnectionService } from '../shared/services/internal-connection.service';
import { ConnectionDTO } from '../connections/dtos/ConnectionDTO';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SearchEntryDTO } from '../connections/dtos/SearchEntryDTO';

@Component({
  selector: 'app-explorer',
  templateUrl: './explorer.component.html',
  styleUrls: ['./explorer.component.css'],
})
export class ExplorerComponent implements OnInit {
  connectionId: String;

  public entries = [];

  activeConnection: ConnectionDTO;

  formGroup: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private entryService: EntriesService,
    private _formBuilder: FormBuilder,
    private internalConnectionService: InternalConnectionService
  ) {}

  ngOnInit() {
    this.formGroup = this._formBuilder.group({
      nameCtrl: [''],
      valueCtrl: [''],
    });
    this.getInternalConnections();
    this.connectionId = this.activeConnection.id;
    if (this.connectionId) this.getEntries();
  }

  exampleMethodParent($event) {
    this.getEntries();
  }

  getEntries() {
    this.entryService.getEntries(this.connectionId).subscribe(
      (data: any) => {
        this.entries = data;
      },
      (error) => {
        alert('error');
        console.log(error);
      }
    );
  }

  getInternalConnections() {
    this.activeConnection = this.internalConnectionService.getConnections();
  }

  search() {
    let searchString =
      this.formGroup.controls['nameCtrl'].value +
      '=' +
      this.formGroup.controls['valueCtrl'].value;
    let params = new SearchEntryDTO();
    params.entryDN = searchString;
    params.connectionId = this.connectionId;
    this.entryService.searchEntries(params).subscribe(
      (data: any) => {
        this.entries = data;
      },
      (error) => {
        alert('error');
        console.log(error);
      }
    );
  }
}
