import { Injectable, Output, EventEmitter } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { FullEntryDTO } from '../../dtos/FullEntryDTO';

@Injectable({
  providedIn: 'root',
})
export class AttributesService {
  attributes: any = [];
  eventEmmiter = new EventEmitter();

  private entrySource = new BehaviorSubject('');
  currentEntryDn = this.entrySource.asObservable();

  private entryConvertedDnSource = new BehaviorSubject('');
  entryConvertedDn = this.entryConvertedDnSource.asObservable();

  constructor() {}

  changeEntryDn(entryDn: any) {
    this.entrySource.next(entryDn);
  }

  changeConvertedDn(dn: any) {
    this.entryConvertedDnSource.next(dn);
  }

  setAttributes(attributes) {
    let attrs = [];
    for (let i = 0; i < attributes.length; i++) {
      if (attributes[i].includes('\n')) {
        let temp = attributes[i].split('\n');
        for (let j = 0; j < temp.length; j++) {
          attrs.push(temp[j]);
        }
      } else {
        attrs.push(attributes[i]);
      }
    }
    this.attributes = attrs;
    this.eventEmmiter.emit(this.attributes);
  }

  getAttributes() {
    return this.attributes;
  }
}
