import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConnectionStrings } from '../../shared/ConnectionStrings';
import { SearchEntryDTO } from '../../connections/dtos/SearchEntryDTO';
import { AddAttributeDTO } from '../../dtos/AddAttributeDTO';
import { AddEntryDTO } from '../../dtos/AddEntryDTO';
import { DeleteEntryDTO } from '../../dtos/DeleteEntryDTO';
import { RenameEntryDTO } from '../../dtos/RenameEntryDTO';
import { RemoveAttributeDTO } from '../../dtos/RemoveAttributeDTO';
import { EditAttributeDTO } from '../../dtos/EditAttributeDTO';

@Injectable({
  providedIn: 'root',
})
export class EntriesService {
  entriesUrl: string = ConnectionStrings.ENTRY_STRING;

  constructor(private http: HttpClient) {}

  getEntries(id: String) {
    return this.http.get(`${this.entriesUrl}/${id}`);
  }

  searchEntries(params: SearchEntryDTO) {
    return this.http.post(`${this.entriesUrl}/search`, params);
  }

  addAttribute(params: AddAttributeDTO) {
    return this.http.post(`${this.entriesUrl}/add-attribute`, params);
  }

  removeAttribute(params: RemoveAttributeDTO) {
    return this.http.post(`${this.entriesUrl}/remove-attribute`, params);
  }

  addEntry(params: AddEntryDTO) {
    return this.http.post(`${this.entriesUrl}`, params);
  }

  deleteEntry(params: DeleteEntryDTO) {
    return this.http.post(`${this.entriesUrl}/delete-entry`, params);
  }

  renameEntry(params: RenameEntryDTO) {
    return this.http.post(`${this.entriesUrl}/rename-entry`, params);
  }

  editAttribute(params: EditAttributeDTO) {
    return this.http.post(`${this.entriesUrl}/edit-attribute`, params);
  }
}
