import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { EntriesService } from '../services/entries.service';
import { ActivatedRoute } from '@angular/router';
import { SearchEntryDTO } from '../../connections/dtos/SearchEntryDTO';
import { FullEntryDTO } from '../../dtos/FullEntryDTO';
import { AttributesService } from '../services/attributes.service';
import { MatDialog } from '@angular/material/dialog';
import { InternalConnectionService } from '../../shared/services/internal-connection.service';
import { AddEntryDialogComponent } from '../dialogs/add-entry-dialog/add-entry-dialog.component';
import { DeleteEntryDTO } from '../../dtos/DeleteEntryDTO';
import { Output } from '@angular/core';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css'],
})
export class EntryComponent implements OnInit {
  @Input('entry')
  public entry: FullEntryDTO;

  @Input('index')
  public index;

  @Output() exampleOutput = new EventEmitter<any>();

  clicked: boolean = false;

  public entries = [];

  public preson: boolean = false;

  public folder: boolean = true;

  listOfLdapObjects = new Map([
    ['cn', 'Common Name'],
    ['ou', 'Organizational Unit'],
    ['dc', 'Domain Component'],
  ]);

  constructor(
    private entryService: EntriesService,
    private route: ActivatedRoute,
    private attributesService: AttributesService,
    private dialog: MatDialog,
    private internalConnectionService: InternalConnectionService
  ) {}

  ngOnInit(): void {
    if (
      this.entry.attributes.some((el) =>
        el.toLowerCase().includes('objectclass: person')
      )
    ) {
      this.preson = true;
      this.folder = false;
    }
  }

  convertDn() {
    if (this.listOfLdapObjects.has(this.entry.dn.split('=')[0])) {
      let ret = this.listOfLdapObjects.get(this.entry.dn.split('=')[0]);
      return ret + ' = ' + this.entry.dn.split('=')[1]
    }
    return this.entry.dn;
  }

  expandEntry() {
    if (this.clicked) {
      this.clicked = false;
      this.entries = [];
    } else {
      this.loadChilds();
    }
  }

  loadAttributes() {
    this.attributesService.changeConvertedDn(this.convertDn());
    this.attributesService.changeEntryDn(this.entry);
    this.attributesService.setAttributes(this.entry.attributes);
  }

  loadChilds() {
    let params = new SearchEntryDTO();
    params.connectionId = this.internalConnectionService.getConnections().id;
    params.entryDN = this.entry.fullDn;
    this.entryService.searchEntries(params).subscribe(
      (data: any) => {
        console.log(data);
        this.entries = data;
        if (data.length > 0) {
          this.clicked = true;
        }
      },
      (error) => {
        alert('error');
        console.log(error);
      }
    );
  }

  addEntry() {
    const dialogRef = this.dialog.open(AddEntryDialogComponent, {
      data: { parentEntry: this.entry, rename: true },
    });
    dialogRef.afterClosed().subscribe(() => {
      if (dialogRef.componentInstance.isDataAdded) this.loadChilds();
    });
  }

  renameEntry() {
    const dialogRef = this.dialog.open(AddEntryDialogComponent, {
      data: {
        parentEntry: this.entry.fullDn,
        entryDn: this.entry.dn,
        attributes: this.entry.fullDn,
        rename: false,
      },
    });
    dialogRef.afterClosed().subscribe(() => {
      if (dialogRef.componentInstance.isDataAdded) this.loadChilds();
    });
  }

  exampleMethodChild() {
    this.exampleOutput.emit('uz');
  }

  exampleMethodParent($event) {
    this.loadChilds();
  }

  deleteEntry() {
    let params = new DeleteEntryDTO();
    params.connectionId = this.internalConnectionService.getConnections().id;
    params.dn = this.entry.fullDn;
    this.entryService.deleteEntry(params).subscribe(
      (resp) => {},
      (error) => {
        console.log(error);
      }
    );
    this.exampleOutput.emit('delete');
  }
}
