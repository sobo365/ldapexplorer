import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicationRoutingModule } from './application-routing.module';
import { FrameComponent } from './frame/frame.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { ExplorerComponent } from './explorer/explorer.component';
import { ConnectionsComponent } from './connections/connections.component';
import { ConnectionComponent } from './connections/connection/connection.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConnectionDialogComponent } from './dialogs/connection-dialog/connection-dialog.component';
import { HomeComponent } from './home/home.component';
import { HelpComponent } from './help/help.component';
import { LogComponent } from './log/log.component';
import { LoggerComponent } from './logger/logger.component';
import { EntryComponent } from './explorer/entry/entry.component';
import { AttributesComponent } from './explorer/attributes/attributes.component';
import { AddAttributeDialogComponent } from './explorer/dialogs/add-attribute-dialog/add-attribute-dialog.component';
import { AddEntryDialogComponent } from './explorer/dialogs/add-entry-dialog/add-entry-dialog.component';
import { RdnFormComponent } from './explorer/dialogs/add-entry-dialog/rdn-form/rdn-form.component';
import { MatTreeModule } from '@angular/material/tree';
import { AttributeItemComponent } from './explorer/attributes/attribute-item/attribute-item.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

@NgModule({
  declarations: [
    FrameComponent,
    TopBarComponent,
    ExplorerComponent,
    ConnectionsComponent,
    ConnectionComponent,
    ConnectionDialogComponent,
    HomeComponent,
    HelpComponent,
    LogComponent,
    LoggerComponent,
    EntryComponent,
    AttributesComponent,
    AddAttributeDialogComponent,
    AddEntryDialogComponent,
    RdnFormComponent,
    AttributeItemComponent,
  ],
  imports: [
    CommonModule,
    ApplicationRoutingModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatStepperModule,
    MatRadioModule,
    MatCheckboxModule,
    MatTreeModule,
    MatAutocompleteModule,
  ],
})
export class ApplicationModule {}
