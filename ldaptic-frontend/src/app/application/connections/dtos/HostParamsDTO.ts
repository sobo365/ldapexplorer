export class HostParametersDTO {
    public hostname: String;
    public port: String;
}