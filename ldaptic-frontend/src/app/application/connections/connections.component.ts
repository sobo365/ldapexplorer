import { Component, OnInit } from '@angular/core';
import { ConnectionDTO } from './dtos/ConnectionDTO';
import {
  MatDialogRef,
  MatDialog,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { ConnectionDialogComponent } from '../dialogs/connection-dialog/connection-dialog.component';
import { ConnectionsService } from './services/connections.service';

@Component({
  selector: 'app-connections',
  templateUrl: './connections.component.html',
  styleUrls: ['./connections.component.css'],
})
export class ConnectionsComponent implements OnInit {
  constructor(
    private connectionsService: ConnectionsService,
    private dialog: MatDialog
  ) {}

  public connections: ConnectionDTO[] = [];

  ngOnInit(): void {
    this.connectionsService.getConnections().subscribe(
      (data: any) => {
        this.connections = data.body;
        console.log(data.body);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  openDialog() {
    const dialogRef = this.dialog.open(ConnectionDialogComponent);
    dialogRef.afterClosed().subscribe((result) => this.ngOnInit());
  }
}
