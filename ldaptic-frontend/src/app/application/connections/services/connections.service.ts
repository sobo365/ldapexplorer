import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';
import { ParametersDTO } from '../../dtos/ParametersDTO';
import { ConnectionDTO } from '../dtos/ConnectionDTO';
import { HostParametersDTO } from '../dtos/HostParamsDTO';

@Injectable({
  providedIn: 'root',
})
export class ConnectionsService {
  connectionsUrl: string = 'http://localhost:8080/connections';

  constructor(private http: HttpClient) {}

  getConnections(): Observable<HttpResponse<ConnectionDTO>> {
    return this.http.get<ConnectionDTO>(this.connectionsUrl, {
      observe: 'response',
    });
  }

  addConnection(parameters: ParametersDTO): Observable<ParametersDTO> {
    return this.http.post<ParametersDTO>(this.connectionsUrl, parameters);
  }

  testConnection(params: HostParametersDTO) {
    return this.http.post(`${this.connectionsUrl}/test`, params);
  }

  deleteConnection(id: String): Observable<{}> {
    const url = `${this.connectionsUrl}/${id}`;
    return this.http.delete(url);
  }
}
