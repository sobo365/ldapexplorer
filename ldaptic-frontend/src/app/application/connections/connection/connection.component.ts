import { Component, OnInit, Input } from '@angular/core';
import { ConnectionDTO } from '../dtos/ConnectionDTO';
import { Router } from '@angular/router';
import { ConnectionsService } from '../services/connections.service';
import { InternalConnectionService } from '../../shared/services/internal-connection.service';

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.css'],
})
export class ConnectionComponent implements OnInit {
  @Input('connection')
  public connection: ConnectionDTO;

  constructor(
    private connectionsService: ConnectionsService,
    private internamConnectionService: InternalConnectionService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  openConnection() {
    this.internamConnectionService.addConnection(this.connection);
    this.router.navigate(['/explorer']);
  }

  deleteConnection() {
    this.connectionsService.deleteConnection(this.connection.id).subscribe();
  }
}
