import { Injectable, EventEmitter } from '@angular/core';
import { ConnectionDTO } from '../../connections/dtos/ConnectionDTO';

@Injectable({
  providedIn: 'root',
})
export class InternalConnectionService {
  activeConnection: ConnectionDTO;
  eventEmmiter = new EventEmitter();

  constructor() {}

  addConnection(connection) {
    this.activeConnection = connection;
  }

  getConnections() {
    return this.activeConnection;
  }
}
