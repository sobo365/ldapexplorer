import { TestBed } from '@angular/core/testing';

import { InternalConnectionService } from './internal-connection.service';

describe('InternalConnectionService', () => {
  let service: InternalConnectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InternalConnectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
